package main

import (
	"bitbacket.com/srv/consts"
	"bitbacket.com/srv/currency"
	"bitbacket.com/srv/pg"
	"bitbacket.com/srv/transport"
	"bitbacket.com/srv/transport/grpc"
	"bitbacket.com/srv/transport/pb"
	"context"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	kitoc "github.com/go-kit/kit/tracing/opencensus"
	kitgrpc "github.com/go-kit/kit/transport/grpc"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/oklog/oklog/pkg/group"
	gg "google.golang.org/grpc"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"net"
	"os"
	"os/signal"
	"syscall"
)

const configFile = "../consts/const.yml"

var (
	logger log.Logger
	cnf    consts.Config
)

func init() {
	var (
		file []byte
		err  error
	)

	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = level.NewFilter(logger, level.AllowDebug())
		logger = log.With(logger,
			"svc", cnf.App.Name,
			"ts", log.DefaultTimestampUTC,
			"clr", log.DefaultCaller,
		)
	}

	file, err = ioutil.ReadFile(configFile)
	if err != nil {
		fmt.Printf("error is : %v \n", err)
	}

	err = yaml.Unmarshal(file, &cnf)
	if err != nil {
		fmt.Printf("error is : %v \n", err)
	}
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	level.Info(logger).Log("msg", "service started")
	defer level.Info(logger).Log("msg", "service ended")

	db, err := sqlx.Connect(cnf.DB.Driver, cnf.DB.Source)
	if err != nil {
		level.Error(logger).Log(err.Error())
		cancel()
	}
	if db == nil {
		cancel()
		panic("DataBase is nil")
	}

	err = db.Ping()
	if err != nil {
		level.Error(logger).Log(err.Error())
	} else {
		level.Info(logger).Log("msg", "data base pinged success")
	}

	defer func() {
		if err = db.Close(); err != nil {
			level.Error(logger).Log(err.Error())
		}

		level.Info(logger).Log("msg", "data base success close")
	}()

	var svc currency.Service
	{
		repo := pg.New(db)
		ad := currency.APIData{
			Link: cnf.API.Link,
			Key:  cnf.API.Key,
		}
		svc = currency.NewService(repo, ad, logger)
	}

	var endpoints transport.Endpoints
	{
		endpoints = transport.MakeEndpoints(svc)
	}

	var (
		ocTracing       = kitoc.GRPCServerTrace()
		serverOptions   = []kitgrpc.ServerOption{ocTracing}
		currencyService = grpc.NewGRPCServer(endpoints, serverOptions, logger)
		grpcServer      = gg.NewServer()
	)

	var port string
	{
		port = ":" + cnf.App.Port
	}

	// в случае , если порт занят и есть какие-либо ошибки , то откючаем
	l, err := net.Listen("tcp", port)
	if err != nil {
		panic(err)
	}

	var g group.Group
	{
		g.Add(func() error {
			logger.Log("transport", "grpc", "addr", port)
			pb.RegisterCurrencyServer(grpcServer, currencyService)

			return grpcServer.Serve(l)
		}, func(error) {
			if err = l.Close(); err != nil {
				level.Error(logger).Log("listener close error", err.Error())
			}

			level.Info(logger).Log("msg", "success close listener")
		})

	}
	{
		cancelInterrupt := make(chan struct{})
		g.Add(func() error {
			c := make(chan os.Signal, 1)
			signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
			select {
			case sig := <-c:
				cancel()
				return fmt.Errorf("received signal %s", sig)
			case <-cancelInterrupt:
				return nil
			}
		}, func(error) {
			close(cancelInterrupt)
		})

	}
	go svc.UpdateCurrencyState(ctx, cnf.App.Timeout)

	level.Error(logger).Log("exit", g.Run())
}
