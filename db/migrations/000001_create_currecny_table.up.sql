create table public.currency (
    id                  bigserial                                       primary key,

    pair                varchar(55)                         not null    unique,

    value               varchar(255)    default ''          not null,

    created_at          date            default now()       not null,
    updated_at          date            default now()       not null
);