package grpc

import (
	"context"
	"strconv"

	"bitbacket.com/srv/transport"
	"bitbacket.com/srv/transport/pb"

	"github.com/go-kit/kit/log"
	kitgrpc "github.com/go-kit/kit/transport/grpc"
	oldcontext "golang.org/x/net/context"
)

type grpcServer struct {
	createCurrency kitgrpc.Handler
	logger         log.Logger
}

// NewGRPCServer returns a new gRPC service for the provided Go kit endpoints
func NewGRPCServer(
	endpoints transport.Endpoints, options []kitgrpc.ServerOption,
	logger log.Logger,
) pb.CurrencyServer {
	errorLogger := kitgrpc.ServerErrorLogger(logger)
	options = append(options, errorLogger)

	return &grpcServer{
		createCurrency: kitgrpc.NewServer(
			endpoints.CreateCurrency, decodeCreateCustomerRequest, encodeCreateCustomerResponse, options...,
		),
		logger: logger,
	}
}

// CreateCustomer Generate glues the gRPC method to the Go kit service method
func (s *grpcServer) CreateCustomer(ctx oldcontext.Context, req *pb.CreateCurrencyRequest) (*pb.CreateCurrencyResponse, error) {
	_, rep, err := s.createCurrency.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}

	return rep.(*pb.CreateCurrencyResponse), nil
}

// decodeCreateCustomerRequest decodes the incoming grpc payload to our go kit payload
func decodeCreateCustomerRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(*pb.CreateCurrencyRequest)

	return transport.CreateCurrencyRequest{
		FirstCurrency:  req.FirstCurrency,
		SecondCurrency: req.SecondCurrency,
	}, nil
}

// encodeCreateCustomerResponse encodes the outgoing go kit payload to the grpc payload
func encodeCreateCustomerResponse(_ context.Context, response interface{}) (interface{}, error) {
	res := response.(transport.CreateCurrencyResponse)

	return &pb.CreateCurrencyResponse{
		Id:  strconv.Itoa(int(res.ID)),
		Msg: res.Msg,
	}, nil
}