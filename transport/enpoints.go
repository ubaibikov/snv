package transport

import (
	"context"

	"bitbacket.com/srv/currency"
	"github.com/go-kit/kit/endpoint"
)

type Endpoints struct {
	CreateCurrency endpoint.Endpoint
}

func MakeEndpoints(s currency.Service) Endpoints {
	return Endpoints{
		CreateCurrency: makeCreateCustomerEndpoint(s),
	}
}

func makeCreateCustomerEndpoint(s currency.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateCurrencyRequest)
		o, _ := s.CreateCurrency(ctx, req.FirstCurrency, req.SecondCurrency)
		return CreateCurrencyResponse{
			ID:  o,
			Msg: "msg",
		}, nil
	}
}
