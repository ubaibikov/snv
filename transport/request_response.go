package transport

type (
	CreateCurrencyRequest struct {
		FirstCurrency  string
		SecondCurrency string
	}

	CreateCurrencyResponse struct {
		ID  int64
		Msg string
	}
)
