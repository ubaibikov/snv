package pg

import (
	"context"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
)

type Repository struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *Repository {
	return &Repository{
		db: db,
	}
}

func (r *Repository) CreateCurrency(ctx context.Context, pair string) (int64, error) {
	var id int64
	q := `
		insert into public.currency(pair) values($1)
		returning id;
	`
	err := r.db.GetContext(ctx, &id, q, pair)
	if err != nil {
		return id, err
	}

	return id, nil
}

func (r *Repository) GetAllPairs(ctx context.Context) ([]string, error) {
	var pairs []string
	q := `
		select array_agg(pair) pairs from currency
	`
	err := r.db.QueryRowContext(ctx, q).Scan(pq.Array(&pairs))
	if err != nil {
		return nil, err
	}

	return pairs, nil
}

func (r *Repository) UpdateByPair(ctx context.Context, rate string, updatedAt, pair string) error {
	q := `
		update currency set value = $1 , updated_at = $2::date where pair = $3 	
	`
	_, err := r.db.ExecContext(ctx, q, rate, updatedAt, pair)
	if err != nil {
		return err
	}

	return nil
}
