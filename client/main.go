package main

import (
	"bitbacket.com/srv/transport/pb"
	"context"
	"fmt"
	"os"

	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
)

const target string = "127.0.0.1:5051"

func main() {
	var (
		opts = []grpc.DialOption{
			grpc.WithInsecure(),
		}
		args = os.Args
	)

	conn, err := grpc.Dial(target, opts...)
	if err != nil {
		grpclog.Fatalf("fail to dial: %v", err)
	}

	defer func() {
		if err = conn.Close(); err != nil {
			fmt.Printf("error when close connectin %v \n", err)
		}
	}()

	client := pb.NewCurrencyClient(conn)
	request := &pb.CreateCurrencyRequest{
		FirstCurrency:  args[1],
		SecondCurrency: args[2],
	}

	response, err := client.CreateCustomer(context.Background(), request)
	if err != nil {
		grpclog.Fatalf("fail to dial: %v", err)
	}

	fmt.Println(response)
}
