package currate

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"io"
	"net/http"
	"time"
)

// Currate update data information with duck interface
type Currate interface {
	GetAll(ctx context.Context) ([]string, error)
	Update(ctx context.Context, pair, rate string) error
}

// PingCurAPI struct for initialize http methods for api
type PingCurAPI struct {
	up Currate
	a  AppInitial
	l  log.Logger
}

// AppInitial initial setting for currate pkg
type AppInitial struct {
	Timeout int
	APIKey  string
	APILink string
}

// New construct for use methods currate api
func New(c Currate, app AppInitial, logger log.Logger) PingCurAPI {
	return PingCurAPI{
		up: c,
		a:  app,
		l:  logger,
	}
}

func (c PingCurAPI) Update(ctx context.Context) {
	isLimit := make(chan bool, 1)
	defer close(isLimit)
	go appealLimit(ctx, isLimit, c.l)
	for {
		select {
		case <-ctx.Done():
			level.Info(c.l).Log("msg", "ctx is donned")
			return
		case <-time.After(time.Second * time.Duration(c.a.Timeout)):
			crs, err := c.up.GetAll(ctx)
			if err != nil {
				level.Error(c.l).Log("currate fatal", err.Error())
				return
			}

			ul := len(crs) - 1 // update limit

			for i, v := range crs {
				if ul == i {
					isLimit <- true
					break
				}

				err = c.updateCurrencyState(ctx, v)
				if err != nil {
					level.Error(c.l).Log("currate fatal", err.Error())
					return
				}
			}

			level.Info(c.l).Log("msg", "pairs up to updated")

		}
	}
}

func appealLimit(ctx context.Context, limit chan bool, logger log.Logger) {
	for {
		select {
		case <-ctx.Done():
			return
		case d := <-limit:
			if d {
				time.Sleep(time.Second * 10)
				level.Info(logger).Log("msg", "slept")
				limit <- false
			}
		default:

		}
	}
}

// curBody http api body
type curBody struct {
	Status  int               `json:"status"`
	Message string            `json:"message"`
	Data    map[string]string `json:"data"`
}

func (c PingCurAPI) updateCurrencyState(ctx context.Context, pair string) error {
	body, err := getCurrentAPIHTTPBody(c.a.APILink, pair, c.a.APIKey)
	if err != nil {
		return err
	}

	defer func(Body io.ReadCloser) {
		err = Body.Close()
		if err != nil {
			return
		}
	}(body)

	b, err := io.ReadAll(body)
	if err != nil {
		return err
	}

	var cb curBody
	err = json.Unmarshal(b, &cb)
	if err != nil {
		return err
	}

	rate, ok := cb.Data[pair]
	if !ok {
		return fmt.Errorf("pair is not valid %s", pair)
	}

	err = c.up.Update(ctx, pair, rate)
	if err != nil {
		return err
	}

	return nil
}

func getCurrentAPIHTTPBody(link, pair, key string) (io.ReadCloser, error) {
	apiLink := fmt.Sprintf("%s&pairs=%s&key=%s", link, pair, key)
	r, err := http.Get(apiLink)
	if err != nil {
		return nil, err
	}

	return r.Body, nil
}
