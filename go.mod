module bitbacket.com/srv

go 1.15

require (
	github.com/go-kit/kit v0.10.0
	github.com/jmoiron/sqlx v1.3.3
	github.com/lib/pq v1.10.1
	github.com/oklog/oklog v0.3.2
	golang.org/x/net v0.0.0-20210428140749-89ef3d95e781
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v2 v2.4.0
)
