migrate create: $(name)
	migrate create -ext sql -dir db/migrations -seq $(name)

migrate up:
	migrate -database postgres://postgres:123@localhost:5432/rtknm?sslmode=disable -path db/migrations up


migrate down:
	migrate -database postgres://postgres:123@localhost:5432/rtknm?sslmode=disable -path db/migrations down

