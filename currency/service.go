package currency

import (
	"context"
	"github.com/go-kit/kit/log"
)

type Service interface {
	CreateCurrency(ctx context.Context, cur1, cur2 string) (int64, error)
	srvUpd
}

type srvUpd interface {
	UpdateCurrencyState(ctx context.Context, timeout int)
}

type Srv struct {
	repo   Repository
	a      APIData
	logger log.Logger
}

type APIData struct {
	Link string
	Key  string
}

func NewService(repo Repository, a APIData, l log.Logger) Service {
	return &Srv{
		repo:   repo,
		a:      a,
		logger: l,
	}
}
