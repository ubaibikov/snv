package currency

import (
	"bitbacket.com/srv/pkg/currate"
	"context"
	"sync"
	"time"
)

// Repository интерфес имплеметированных методов репозитория
type Repository interface {
	CreateCurrency(ctx context.Context, pair string) (int64, error)
	GetAllPairs(ctx context.Context) ([]string, error)
	UpdateByPair(ctx context.Context, rate string, updatedAt, pair string) error
}

// CreateCurrency создание currency
func (s *Srv) CreateCurrency(ctx context.Context, cur1, cur2 string) (int64, error) {
	return s.repo.CreateCurrency(ctx, cur1+cur2)
}

// crt интерфес для для получения данных и , собственно, их данных
type crt interface {
	Update(ctx context.Context)
}

var wg sync.WaitGroup

// GetAll получение всевозможных пар
func (s *Srv) GetAll(ctx context.Context) ([]string, error) {
	return s.repo.GetAllPairs(ctx)
}

// Update обнолвение пары
func (s *Srv) Update(ctx context.Context, pair, rate string) error {

	dn := time.Now().Format("01-02-2006")

	return s.repo.UpdateByPair(ctx, rate, dn, pair)
}

// UpdateCurrencyState обноляет состояние путем api
func (s *Srv) UpdateCurrencyState(ctx context.Context, timeout int) {
	a := currate.AppInitial{
		APILink: s.a.Link,
		APIKey:  s.a.Key,
		Timeout: timeout,
	}

	var ca crt = currate.New(s, a, s.logger)

	wg.Add(1)
	defer wg.Done()

	go func() {
		ca.Update(ctx)
	}()
}
