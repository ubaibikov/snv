package consts

type Config struct {
	App    	app `yaml:"app"`
	DB     	db  `yaml:"db"`
	DBTest 	db  `yaml:"db_test"`
	API 	api `yaml:"api"`
}

type db struct {
	Source string `yaml:"source"`
	Driver string `yaml:"driver"`
}

type app struct {
	Name    string `yaml:"name"`
	Port    string `yaml:"port"`
	Timeout int    `yaml:"timeout"`
}

type api struct {
	Link 	string	`yaml:"link"`
	Key 	string 	`yaml:"key"`
}